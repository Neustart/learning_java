package com.company;

import java.io.*;
import java.util.Random;

/**
 * ArraySorter
 * @version v1.0
 * @author Kirill Kosteneckiy
 *
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException {


        float Mas[] = File.read(args[1]);
        SortingOut sort_o;

        MyArray.print(Mas);

        switch (args[0]){
            case "bubble":
                sort_o = new Bubble();
                break;
            case "quick":
                sort_o = new Qsort();
                break;
            case "shift":
                sort_o = new Shift();
                break;
            default:
                sort_o = new Shift();
                System.exit(1);
                break;
        }

        sort_o.sort(Mas);

        System.out.println();
        MyArray.print(Mas);

    }

    public static class File {
        public static float[] read(String Filename) throws FileNotFoundException {
            
            StringBuilder sb = new StringBuilder();

            try {
               
                BufferedReader in = new BufferedReader(new FileReader(Filename));
                try {
                    
                    String s;
                    while ((s = in.readLine()) != null) {
                        sb.append(s);
                        sb.append("\n");
                    }
                } finally {
                    
                    in.close();
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
            }

            String input_values=sb.toString();

            int i=0;
            for (String retval: input_values.split("\n")){
                i++;
            }
            float Mas[] = new float[i];
            int k =0;
            for (String retval: input_values.split("\n")){
                try {
                    Mas[k] = Integer.valueOf(retval);
                    k++;
                }catch (NumberFormatException e) {
                    System.err.println("Неверный формат ввода");
                    System.exit(1);
                }
                catch (Exception e){
                    System.err.println("Ошибка.");
                    System.exit(2);
                }
            }
            return Mas;
        }
    }
    public static class MyArray {
        public static void  print(float[] Mas) {
            for (int i = 0; i < Mas.length; i++) {
                System.out.print(Mas[i]+" ");
            }
        }
    }
}
    class Qsort implements SortingOut {

        @Override
        public void sort(float[] array) {
            int l = 0;
            int r = array.length - 1;
            sort_more_arguments(array,l,r);
        }
        public void sort_more_arguments(float[] array, int l, int r) {
            int i = l;
            int j = r;
            Random random = new Random();
            float x = array[l + random.nextInt(r - l + 1)];
            while (i <= j) {
                while (array[i] < x) {
                    i++;
                }
                while (array[j] > x) {
                    j--;
                }
                if (i <= j) {
                    float temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    i++;
                    j--;
                }
            }
            if (l < j) {
                sort_more_arguments(array, l, j);
            }
            if (i < r) {
                sort_more_arguments(array, i, r);
            }
        }
    }
    class Bubble implements SortingOut {
        @Override
        public void sort(float[] array) {
            for (int i = 0; i < array.length-1; i++){
                for (int j = i+1; j < array.length; j++){
                    if (array[i] > array[j]) {
                        float t = array[i];
                        array[i] = array[j];
                        array[j] = t;
                    }
                }
            }
        }
    }
    class Shift implements SortingOut {
        @Override
        public void sort(float[] array) {
            for(int i = 1; i < array.length; i++){
                float cur = array[i];
                int prev = i - 1;
                while(prev >= 0 && array[prev] > cur){
                    array[prev+1] = array[prev];
                    array[prev] = cur;
                    prev--;
                }
            }
        }
    }

interface SortingOut {
    void sort(float[] array);

}


