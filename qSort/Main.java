package com.company;

import java.util.Random;
import java.util.Scanner;
/**
 * qSort
 * @version v1.0 2015.03.30
 * @author Kirill Kosteneckiy
 *
 */
public class Main {

    public static void main(String[] args) {
        String input_values;
        Scanner in = new Scanner(System.in);
        //Ввод строки и конвертирование ее в массив
        int Mas[] = MyArray.String_to_Mas(in.next());
        // Вывод и сортировка массива
        MyArray.print(Mas);
        MyArray.qSort(Mas,0,Mas.length-1);
        //Вывод массива после сортировки
        System.out.println();
        MyArray.print(Mas);

    }
    public static class MyArray{

        public static int[] String_to_Mas(String input_values){
            int i=0;
            for (String retval: input_values.split(",")){
                i++;
            }
            int Mas[] = new int[i];

            int k =0;
            for (String retval: input_values.split(",")){
                try {
                    Mas[k] = Integer.valueOf(retval);
                    k++;
                }catch (NumberFormatException e) {
                    System.err.println("Неверный формат строки. Числа стоит вводить через запятую.");
                    System.exit(1);
                }
                catch (Exception e){
                    System.err.println("Ошибка.");
                    System.exit(2);
                }
            }
            return Mas;
        };
        public static void  print(int[] Mas) {
            for (int i = 0; i < Mas.length; i++) {
                System.out.print(Mas[i]+" ");
            }
        }
        public static void qSort(int[] array, int l, int r) {
            int i = l;
            int j = r;
            Random random = new Random();
            int x = array[l + random.nextInt(r - l + 1)];
            while (i <= j) {
                while (array[i] < x) {
                    i++;
                }
                while (array[j] > x) {
                    j--;
                }
                if (i <= j) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    i++;
                    j--;
                }
            }
            if (l<j){
                qSort(array, l, j);
            }
            if(i<r){
                qSort(array, i, r);
            }
        }
    }
}
