package com.company;

/**
 * Calculator
 * @version v1.0 2015.03.05
 * @author Kirill Kosteneckiy
 *
 */
public class Main {

    public static void main(String[] args) {

        try {
            double a = Double.valueOf(args[0]);
            double b = Double.valueOf(args[1]);
            if (args[0].length() < 10 && args[1].length() < 10 ) {
                new Calculator(a, b, args[2]);
            }
            else {
                System.err.println("Error: Max size of input values - ten numbers");
                System.exit(0);
            }
        }
        catch (NumberFormatException e) {
            System.err.println("Error: use int or double values");
        }
        catch (Exception e){
            System.err.println("Error: " + e.toString());
        }
    }
    public static class Calculator  {

        public Calculator(double a, double b, String o){
            if (o.equals("add")) {
                System.out.println(a + " + " + b + " = " + add(a,b));
                return;
            }
            if (o.equals("sub")){
                System.out.println(a + " - " + b + " = " + sub(a,b));
                return;
            }
            if (o.equals("mul")){
                System.out.println(a + " * " + b + " = " + mul(a,b));
                return;
            }
            if (o.equals("div")){
                System.out.println(a + " / " + b + " = " + div(a,b));
                return;
            }if (o.equals("mod")){
                System.out.println(a + " % " + b + " = " + mod(a,b));
                return;
            }
            System.err.println("Error: you can use only add/sub/mul/div/mod operations");
        }

        public double add(double a, double b){
            return a + b;
        }
        public double sub(double a, double b){
            return a - b;
        }
        public double mul(double a, double b){
            return a * b;
        }
        public double div(double a, double b){
            return a / b;
        }
        public double mod(double a, double b){
            return a % b;
        }
    }
}
